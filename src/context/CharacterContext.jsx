import { createContext, useEffect, useState } from "react";
import Axios from 'axios';
export const CharacterContext = createContext();

export const CharacterContextProvider = ({ children }) => {
    const [characters, setCharacters] = useState([]);
    const [results, setResults] = useState([]);
    const [search, setSearch] = useState("");
    const [page, setPage] = useState(1);
    const [loadMore, setLoadMore] = useState(false);
    const [loading, setLoading] = useState(false);

    const delay = ms => new Promise(
        resolve => setTimeout(resolve, ms)
    );

    useEffect(() => {
        Axios.get('https://rickandmortyapi.com/api/character/')
            .then(response => {
                if (response.status === 200) {
                    setCharacters(response.data);
                    setResults(response.data.results);
                } else {
                    setCharacters({});
                    setResults([]);
                }
            })
    }, []);

    useEffect(() => {
        setPage(1);
        Axios.get('https://rickandmortyapi.com/api/character/?page=' + page + '&name=' + search)
            .then(response => {
                if (response.status === 200) {
                    setCharacters(response.data);
                    setResults(response.data.results);
                } else {
                    setCharacters({});
                    setResults([]);
                }
            })
    }, [search]);

    // useEffect(() => {
    //     Axios.get('https://rickandmortyapi.com/api/character/?page=' + page + '&name=' + search)
    //         .then(response => {
    //             if (response.status === 200) {
    //                 setCharacters(response.data);
    //                 setResults(response.data.results);
    //             } else {
    //                 setCharacters({});
    //                 setResults([]);
    //             }
    //         })
    // }, [page]);

    useEffect(() => {
        setPage(page + 1);
        setLoading(true);
        loadMore &&

            Axios.get('https://rickandmortyapi.com/api/character/?page=' + page + '&name=' + search)
                .then(async response => {
                    if (response.status === 200) {
                        await delay(2000);
                        setResults(results => [...results, ...response.data.results]);
                        setLoading(false);
                    }
                })
    }, [loadMore]);

    return (
        <CharacterContext.Provider
            value={
                {
                    characters, setCharacters,
                    search, setSearch,
                    page, setPage,
                    loadMore, setLoadMore,
                    results, setResults,
                    loading, setLoading
                }
            }>
            {children}
        </CharacterContext.Provider>
    )
}