import { Header } from './components/Header'
import { Content } from './components/Content'
import { CharacterContextProvider } from './context/CharacterContext'
import Router from './router/Router'

function App() {
  return (
    <CharacterContextProvider>
      <div className="min-h-screen">
        {/* header */}
        <Header />
        <div className="container mx-auto min-h-screen">
          <Router />
        </div>
      </div>
    </CharacterContextProvider>
  )
}

export default App
