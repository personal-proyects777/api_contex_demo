import React from 'react'
import reactLogo from '../assets/react.svg'
import viteLogo from '../../public/vite.svg'
import { NavLink } from 'react-router-dom'

export const Header = () => {
    return (
        <div className='flex flex-col bg-gray-800 p-3 mb-4 '>
            <div className='flex flex-row'>
                <div className="header flex flex-row mr-2">
                    <img className='mx-2' src={reactLogo} alt="30" />
                </div>
                <div className="title flex-1 flex justify-center">
                    <h2 className='font-semibold text-white text-2xl'>API CONTEX RICK AND MORTHY</h2>
                </div>
                <div className="header flex flex-row mr-2">
                    <img className='mx-2' src={viteLogo} alt="35" />
                </div>
            </div>
            <div className="my-2">
                <NavLink to="/page1" className={({ isActive }) => isActive ?
                    'text-blue-700 p-2 mx-2 hover:bg-slate-600 hover:text-white' : 'text-white p-2 mx-2 hover:bg-slate-600 hover:text-white'
                }>Pagina 1</NavLink>
                <NavLink to="/page2" className={({ isActive }) => isActive ?
                    'text-blue-700 p-2 mx-2 hover:bg-slate-600 hover:text-white' : 'text-white p-2 mx-2 hover:bg-slate-600 hover:text-white'
                }>Pagina 2</NavLink>
            </div>
        </div >
    )
}
