import React, { useContext, useEffect, useMemo, useRef } from 'react'

import { FaArrowLeft, FaArrowRight, FaSearch } from 'react-icons/fa';

import { CharacterContext } from '../context/CharacterContext';

import { Personajes } from './Personajes';
import { Paginations } from './Paginations';

export const Content = () => {
    const { characters, setSearch, page, setPage, setLoadMore, results, loading } = useContext(CharacterContext);

    const myRef = useRef(null);

    const options = useMemo(() => {
        return {
            root: null,
            rootMargin: '0px',
            threshold: 0.3
        }
    }, []);

    const callbackFunction = (entries) => {
        const [entry] = entries;
        setLoadMore(entry.isIntersecting);
        if (entry.isIntersecting) {
            console.log('%cTrayendo mas resultados...%c', 'background-color: yellow, font-size: larger, color: white');
        }

    }


    useEffect(() => {
        const observer = new IntersectionObserver(callbackFunction, options);
        const lastCurrentItem = myRef.current && myRef.current.children[(results.length - 1)];
        if (lastCurrentItem) observer.observe(lastCurrentItem);

        return () => {
            if (lastCurrentItem) observer.unobserve(lastCurrentItem);
        }
    }, [results, options])

    if (!characters.info) { return null; }

    const handleSearch = (event) => {
        setSearch(event.target.value);
    }

    const nextPage = () => {
        setPage(parseInt(page) + 1);
    }
    const prevPage = () => {
        setPage(parseInt(page) - 1);
    }

    return (
        <div className='p-2'>
            <div className="options">
                <div className="flex items-center justify-between pb-4">
                    <div className="">
                        <div className="options flex">

                            {page <= 1 ?
                                <button onClick={prevPage}
                                    disabled='disabled'
                                    className='disabled:opacity-75 flex align-middle justify-center border-2 text-gray-600 bg-gray-50 border-gray-300 px-2 py-1 rounded-xl mr-2'>
                                    <FaArrowLeft className='mr-2 my-auto' />Previus</button>
                                :
                                <button onClick={prevPage}
                                    className='flex align-middle justify-center border-2 text-gray-600 bg-gray-50 border-gray-300 hover:bg-purple-500 hover:text-white px-2 py-1 rounded-xl mr-2'>
                                    <FaArrowLeft className='mr-2 my-auto' />Previus</button>
                            }
                            {characters.info.pages == page ?
                                <button onClick={nextPage}
                                    disabled='disabled'
                                    className='disabled:opacity-75 flex align-middle justify-center border-2 text-gray-600 bg-gray-50 border-gray-300 hover:bg-purple-500 hover:text-white px-2 py-1 rounded-xl mx-2'>
                                    Next<FaArrowRight className='ml-2 my-auto' /></button>
                                :
                                <button onClick={nextPage}
                                    className='flex align-middle justify-center border-2 text-gray-600 bg-gray-50 border-gray-300 hover:bg-purple-500 hover:text-white px-2 py-1 rounded-xl mx-2'>
                                    Next<FaArrowRight className='ml-2 my-auto' /></button>
                            }
                        </div>
                    </div>
                    <div className="flex-1">
                        <Paginations />
                    </div>
                    <div className="relative">
                        <label className="sr-only">Search</label>
                        <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <FaSearch className='text-gray-500' />
                        </div>
                        <input type="text" onChange={handleSearch} id="table-search" className="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-80 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search for items" />
                    </div>
                </div>
            </div>
            {/* <div className="max-h-72 block overflow-y-auto my-10 bg-white">
                <pre>{JSON.stringify(results, null, 2)}</pre>
            </div> */}
            <div ref={myRef} className="grid sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-5 lg:grid-flow-row gap-3">
                {results.map(e => {
                    return (
                        <Personajes key={e.id} data={e} />
                    )
                })}
            </div>
            {loading &&
                <div className='flex m-4 justify-center p-4' role="status">
                    <svg aria-hidden="true" className="w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor" />
                        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill" />
                    </svg>
                    <span className="sr-only">Loading...</span>
                </div>
            }
        </div>
    )
}
