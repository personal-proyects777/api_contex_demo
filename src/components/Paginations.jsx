import React, { useContext } from 'react'

import { CharacterContext } from '../context/CharacterContext';

export const Paginations = () => {

    const { page, setPage, characters } = useContext(CharacterContext);
    const changePage = (e) => {
        let value = e.target.value;
        setPage(value);
    }
    return (
        <div className='flex flex-row justify-between p-1 align-middle'>
            <div className="mx-4">
                <p>Total Results: {characters.info.count}</p>
            </div>
            <div className="mx-4">
                <p>Page {page} of {characters.info.pages}</p>
            </div>
            {/* <div className="mx-4 flex-row flex align-middle">
                <p className='mr-2'>Ir a la pagina</p>
                <select value={page} onChange={changePage} className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-1.5'>
                    {Array.from(Array(characters.info.pages).keys()).map((e, i) => {
                        return <option key={i} value={e + 1}>{e + 1}</option>
                    })}
                </select>
            </div> */}
        </div>
    )
}
