import React from 'react'

export const Personajes = ({ data }) => {
    return (
        <div className='relative w-76 lg:w-auto bg-gray-50 rounded-lg shadow-lg'>
            <img className="w-full mx-auto rounded-t-lg" src={data.image} alt="Sunset in the mountains"></img>
            <div className="status absolute top-0 right-0 bg-gray-600 py-1 px-2 rounded-bl-md border-gray-400 border-1">
                <span className='flex align-middle'>
                    {data.status === 'Alive' ?
                        <span className='h-2 w-2 mr-1 rounded-full bg-green-600 my-auto'></span>
                        :
                        <span className='h-2 w-2 mr-1 rounded-full bg-red-600 my-auto'></span>
                    }
                    <p className='font-semibold text-white text-sm'>{data.status}</p>
                </span>
            </div>
            <div className="p-2">
                <div className="font-bold text-xl"><a className='text-gray-950 transition duration-150 ease-in-out hover:text-blue-600 focus:text-blue-600 active:text-primary-700' href="">{data.name}</a></div>
                <div className="info mt-4">
                    <span className=''>
                        <p className='text-sm text-neutral-400'>Origin</p>
                        <a className='text-sm font-semibold text-gray-950 transition duration-150 ease-in-out hover:text-blue-600 focus:text-blue-600 active:text-primary-700' href={data.origin.name}>{data.origin.name}</a>
                    </span>
                    <div className="flex justify-center mb-2">
                        <span className='flex-1'>
                            <p className='text-sm text-neutral-400'>Type</p>
                            <p className='text-sm font-semibold text-gray-950'>{data.species}</p>
                        </span>
                        <span className='flex-1'>
                            <p className='text-sm text-neutral-400'>Gender</p>
                            <p className='text-sm font-semibold text-gray-950'>{data.gender}</p>
                        </span>
                    </div>
                    <span className=''>
                        <p className='text-sm text-neutral-400'>Last known location</p>
                        <a className='text-sm font-semibold text-gray-950 transition duration-150 ease-in-out hover:text-blue-600 focus:text-blue-600 active:text-primary-700' href={data.location.url}>{data.location.name}</a>
                    </span>
                </div>
            </div>
        </div>
    )
}
