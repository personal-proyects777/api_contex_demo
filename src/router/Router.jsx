import React, { Component } from 'react'
import { Route, Routes } from 'react-router-dom'
import { Main } from '../views/Practice1/main'


export class Router extends Component {
    render() {
        return (
            <Routes>
                <Route path='/page1' element={<Main />} />
            </Routes>
        )
    }
}

export default Router